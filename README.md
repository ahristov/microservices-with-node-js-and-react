# Microservices with Node JS and React

This project contains notes and code from studying [Microservices with Node JS and React](https://www.udemy.com/course/microservices-with-node-js-and-react).

## 🏠 [Homepage](https://bitbucket.org/ahristov/microservices-with-node-js-and-react/src/master/)

## Notes

### K8s: Config file: Create pod

Rebuild the docker container, give it a tag with a version ID. Move to "posts" and build an image from current (.) directory:

```sh
cd posts
docker build -t ahristov/posts:0.0.1 .
```

Next we write a k8s config file to create a pod from this image. We create these config files in directory "infra\k8s" with the name of "posts.yaml":

```yaml
apiVersion: v1    # Specifies the set of objects we want k8s to look at. Note: we can add our custom objects
kind: Pod         # Type of the object to create - Pod, Deployment, etc.
metadata:         # Data that helps uniquely identify the object, including a name or UID
  name: posts
spec:             # What state you desire for the object
  containers:     # We can create multiple containers on a single pod
    - name: posts # Name of the image to use
      image: ahristov/posts:0.0.1  # The exact image to use. 
                                   # If version is not specified, it will try to reach docker hub for "latest".
                                   # If version is implicit "latest", it will try to reach docker hub for it.
                                   # If version is specified, it will first look on the same computer.
                                   # Usually we would have "ahristov/posts"
```

Config file directives:

- apiVersion: Specifies the set of objects we want k8s to look at. Note: we can add our custom objects

- kind: Type of object to create

Build the pod:

```sh
$ cd ..\infra\k8s\
$ kubectl apply -f posts.yaml
pod/posts created
```

Inspect all the pods:

```sh
$ kubectl get pods
NAME    READY   STATUS    RESTARTS   AGE
posts   1/1     Running   0          80s
```

Run a command on a pod:

```sh
$ kubectl exec posts -- top -n 1
?[H?[JMem: 3935644K used, 4190680K free, 5316K shrd, 280340K buff, 1940780K cached
CPU:   2% usr   2% sys   0% nic  94% idle   0% io   0% irq   0% sirq
Load average: 0.21 0.15 0.11 2/1031 62
?[7m  PID  PPID USER     STAT   VSZ %VSZ CPU %CPU COMMAND?[m
    1     0 root     S     283m   3%   1   0% {node} npm
   25     1 root     S     276m   3%   3   0% node /app/node_modules/.bin/nodemo
   41    25 root     S     249m   3%   3   0% /usr/local/bin/node index.js
   58     0 root     R     1576   0%   0   0% top -n 1
```

Open a shell in the pod:

```sh
$ kubectl exec -it posts -- sh
/app # ls
Dockerfile         index.js           node_modules       package-lock.json  package.json       yarn.lock
```

View logs:

```sh
kubectl logs posts
```

Delete a pod:

```sh
kubectl delete pod posts
```

View information about a pod:

```sh
kubectl describe pod posts
```

### K8s: Config file: Create deployment

Write a k8s config file to create a deployment from posts image.

Create config file `infra/k8s/posts-depl.yaml`:

```yaml
apiVersion: apps/v1   # Create a bucket "apps/v1"
kind: Deployment      # Below we tell which pods to manage
metadata:
  name: posts-depl
spec:
  replicas: 1         # How many pods we will run
  selector:           # Which pods to manage
    matchLabels:      # Take a look at all the pods that have been created
      app: posts      # -> match
  template:           # Specify the exact configuration of a pod that we want this deployment to create
    metadata:
      labels:
        app: posts    # -> match
    spec:
      containers:
        - name: posts # Name of the image to use
          image: ahristov/posts:0.0.1  # The exact image to use.
```

For resource limits see: [manage-resources-containers](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/)

Apply the file:

```sh
kubectl apply -f .\posts-depl.yaml
```

Get list of deployments:

```sh
$ kubectl get deployments
NAME         READY   UP-TO-DATE   AVAILABLE   AGE
posts-depl   1/1     1            1           26s
```

It shows that "posts-depl" has one pod deployed and it is available.

We can also view all the pods:

```sh
$ kubectl get pods
NAME                          READY   STATUS    RESTARTS   AGE
posts                         1/1     Running   0          38m
posts-depl-5484fd9b9d-2zfrv   1/1     Running   0          2m5s
```

### K8s: Config file: Create service (networking)

#### Node Port Service

Create new file `infra/k8s/posts-srv.yaml`:

```yaml
apiVersion: v1
kind: Service
metadata:
  name: posts-srv
spec:
  type: NodePort
  selector:
    app: posts # what pods should expose
  ports:
  - name: posts # can be different than the name of the pod
    protocol: TCP
    port: 4000
    targetPort: 4000 # the app is listening on this port

```

This exposes a NodePort service.

```sh
$ kubectl apply -f .\posts-srv.yaml
service/posts-srv created

$ kubectl get services
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP          38d
posts-srv    NodePort    10.106.6.247   <none>        4000:30379/TCP   94s

$ kubectl describe service posts-srv
Name:                     posts-srv
Namespace:                default
Labels:                   <none>
Annotations:              <none>
Selector:                 app=posts
Type:                     NodePort
IP:                       10.106.6.247
LoadBalancer Ingress:     localhost
Port:                     posts  4000/TCP
TargetPort:               4000/TCP
NodePort:                 posts  30379/TCP
Endpoints:                10.1.1.4:4000
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>

```

The posts service is now accessible  at “GET http://localhost:30379/posts”.

#### Cluster IP Service

These are the steps for Event Bus.

Create docker file for Event Bus `blog\event-bus\Dockerfile`.

```sh
FROM node:alpine

WORKDIR /app
COPY package.json ./
RUN npm install
COPY ./ ./

CMD ["npm", "start"]
```

Build image for Event Bus. Push the image to Docker Hub.

```sh
cd blog\event-bus
docker build -t atanashristov/evens-bus .
docker push atanashristov/evens-bus
```

Create **deployment** for Event Bus. First create deployment file `infra\k8s\event-bus-depl.yaml`:

```yaml
apiVersion: apps/v1   # Create a bucket "apps/v1"
kind: Deployment      # Below we tell which pods to manage
metadata:
  name: event-bus-depl
spec:
  replicas: 1         # How many pods we will run
  selector:           # Which pods to manage
    matchLabels:      # Take a look at all the pods that have been created
      app: event-bus      # -> match
  template:           # Specify the exact configuration of a pod that we want this deployment to create
    metadata:
      labels:
        app: event-bus    # -> match
    spec:
      containers:
        - name: event-bus # Name of the image to use
          image: atanashristov/evens-bus  # Use "latest"

```

Apply to k8s:

```sh
$ cd ..\infra\k8s\
$ kubectl apply -f .\event-bus-depl.yaml
deployment.apps/event-bus-depl created

$ kubectl get pods
NAME                              READY   STATUS    RESTARTS   AGE
event-bus-depl-6cfbdd6c69-7fznn   1/1     Running   0          33s
posts-depl-779b6d47f7-7ktsk       1/1     Running   0          5h14m
```

Extend the deployment file:

```yaml
apiVersion: apps/v1   # Create a bucket "apps/v1"
kind: Deployment      # Below we tell which pods to manage
metadata:
  name: event-bus-depl
spec:
  replicas: 1         # How many pods we will run
  selector:           # Which pods to manage
    matchLabels:      # Take a look at all the pods that have been created
      app: event-bus      # -> match
  template:           # Specify the exact configuration of a pod that we want this deployment to create
    metadata:
      labels:
        app: event-bus    # -> match
    spec:
      containers:
        - name: event-bus # Name of the image to use
          image: atanashristov/evens-bus  # Use "latest"
---
apiVersion: v1
kind: Service
metadata:
  name: event-bus-srv
spec:
  type: ClusterIP # It is the default
  selector:
    app: event-bus # what pods should expose
  ports:
  - name: event-bus # can be different than the name of the pod
    protocol: TCP
    port: 4005
    targetPort: 4005 # the app is listening on this port

```

Deploy k8s service:

```sh
$ kubectl apply -f .\event-bus-depl.yaml
deployment.apps/event-bus-depl unchanged
service/event-bus-srv created
```

Inspect current services:

```sh
$ kubectl get services
NAME            TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
event-bus-srv   ClusterIP   10.98.249.230   <none>        4005/TCP         5m13s
kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP          38d
posts-srv       NodePort    10.106.6.247    <none>        4000:30379/TCP   4h29m
```
