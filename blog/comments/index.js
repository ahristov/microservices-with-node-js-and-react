const express = require('express')
const bodyParser = require('body-parser')
const { randomBytes } = require('crypto')
const cors = require('cors')
const axios = require('axios')

const app = express()
app.use(bodyParser.json())
app.use(cors())

const commentsByPostId = {};

app.get('/posts/:postId/comments', (req, res) => {
  const { postId } = req.params
  res.send(commentsByPostId[postId] || [])
})

app.post('/posts/:postId/comments', async (req, res) => {
  const commentId = randomBytes(4).toString('hex')
  const { content } = req.body
  const { postId } = req.params

  const comments = commentsByPostId[postId] || []
  comments.push({
    id: commentId,
    content,
    status: 'pending'
  })
  commentsByPostId[postId] = comments

  await axios.post('http://event-bus-clusterip-srv:4005/events', {
    type: 'CommentCreated',
    data: {
      id: commentId,
      content,
      status: 'pending',
      postId
    }
  })

  res.status(201).send(comments)
})

app.post('/events', async (req, res) => {
  const { type, data } = req.body

  if (type === 'CommentModerated') {
    const { id, status, content, postId } = data

    const comments = commentsByPostId[postId]
    const comment = comments.find(comment => {
      return comment.id === id
    })

    comment.status = status

    await axios.post('http://event-bus-clusterip-srv:4005/events', {
      type: 'CommentUpdated',
      data: {
        id,
        content,
        status,
        postId
      }
    })
  }

  res.send({})
})

app.listen(4001, () => {
  console.log("Comments listening on 4001")
})