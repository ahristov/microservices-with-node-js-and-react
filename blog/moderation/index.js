const express = require('express')
const bodyParser = require('body-parser')
const axios = require('axios')

const app = express()
app.use(bodyParser.json())

app.post('/events', async (req, res) => {
  console.log('Received event', req.body)

  const { type, data } = req.body
  const { id, content, postId } = data

  if (type === 'CommentCreated') {
    const status = content.includes('orange')
      ? 'rejected'
      : 'approved'

    await axios.post('http://event-bus-clusterip-srv:4005/events', {
      type: 'CommentModerated',
      data: {
        id,
        content,
        status,
        postId
      }
    })
  }

  res.send({})
})


app.listen(4003, () => {
  console.log("v1");
  console.log("Moderation listening on 4003")
})

